/** @file
    @brief Implementation

    @date 2018

    @author
    Sensics, Inc.
    <http://sensics.com/osvr>
*/

// Copyright 2018 Sensics, Inc.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//        http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

// Internal Includes
#include <osvr/PluginKit/ButtonInterfaceC.h>
#include <osvr/PluginKit/PluginKit.h>
#include <osvr/Util/Logger.h>
extern "C" {
    #include "smbus.h"
}
// Generated JSON header file
#include "com_sensics_proximity_json.h"

// Library/third-party includes
// - none

// Standard includes
#include <linux/i2c-dev.h>
#include <sys/ioctl.h>
#include <fcntl.h>
#include <stdio.h>
#include <unistd.h>

// anonymous namespace
namespace {

#define VCNL4040_DEV_ID 0x186

static const auto PREFIX = "OSVR-ProxSensor";

const uint8_t PROX_SENSOR_SLAVE_ADDRESS = 0x60;

// command codes
const uint8_t PS_MS = 0x04;
const uint8_t PS_CONF1 = 0x03;
const uint8_t PS_CONF2 = 0x03;
const uint8_t ALS_CONF = 0x00;
const uint8_t DEV_ID_CMD = 0x0C;
const uint8_t PS_DATA_CMD = 0x08;

class ProximitySensorDevice {

  public:
    ProximitySensorDevice(OSVR_PluginRegContext ctx, int file)
        : m_logger(osvr::util::log::make_logger(PREFIX)), m_file(file) {

        /// Create the initialization options
        OSVR_DeviceInitOptions opts = osvrDeviceCreateInitOptions(ctx);

        osvrDeviceButtonConfigure(opts, &m_button, 1);

        /// Create the sync device token with the options
        m_dev.initAsync(ctx, "ProxSensor", opts);

        /// Send JSON descriptor
        m_dev.sendJsonDescriptor(com_sensics_proximity_json);

        /// Register update callback
        m_dev.registerUpdateCallback(this);
    }

    ~ProximitySensorDevice() { close(m_file); }

    OSVR_ReturnCode update() {
        int data = i2c_smbus_read_word_data(m_file, PS_DATA_CMD);

        if (data < 0) {
            m_logger->error("Error reading proximity data from device\n");
        } else {
            if (data > m_upperThreshold) {
                // the range is from 0 - ~3500
                // sanity check for wrong values
                if (data < 5000){
                    osvrDeviceButtonSetValue(m_dev, m_button, OSVR_BUTTON_PRESSED, 0);
                    return OSVR_RETURN_SUCCESS;
                }
            }
            if (data <= m_lowerThreshold){
                osvrDeviceButtonSetValue(m_dev, m_button, OSVR_BUTTON_NOT_PRESSED, 0);
            }
        }
        return OSVR_RETURN_SUCCESS;
    }

  private:
    osvr::pluginkit::DeviceToken m_dev;
    osvr::util::log::LoggerPtr m_logger;
    OSVR_ButtonDeviceInterface m_button;

    uint16_t m_upperThreshold = 1500;
    uint16_t m_lowerThreshold = 1100;

    int m_file;

};

class HardwareDetection {
  public:
    HardwareDetection() : m_logger(osvr::util::log::make_logger(PREFIX)), proxSensorDetected(false) {}

    OSVR_ReturnCode operator()(OSVR_PluginRegContext ctx) {

        if (proxSensorDetected) {
            return OSVR_RETURN_SUCCESS;
        }

        int file;
        file = open("/dev/i2c-1", O_RDWR);

        if (file < 0) {
            m_logger->error("Could not open") << "/dev/i2c-1" << std::endl;
            return OSVR_RETURN_FAILURE;
        }

        int rc = ioctl(file, I2C_SLAVE, PROX_SENSOR_SLAVE_ADDRESS);
        if (rc < 0) {
            m_logger->error("Could not set device address");
            return OSVR_RETURN_FAILURE;
        }

        // enable proximity detection logic output mode
        /// todo check if proximity normal operation with interrupt func
        uint16_t cmd = 0;
        cmd |= 0x40;
        int ret = i2c_smbus_write_word_data(file, PS_MS, cmd);

        if (ret != 0) {
            m_logger->error("Received an error code setting PS_MS ") << ret << std::endl;
            return OSVR_RETURN_FAILURE;
        } else {
            m_logger->error("PS_MS Write successful");
        }

        cmd = 0;
        cmd |= 0x800;
        ret = i2c_smbus_write_word_data(file, PS_CONF1, cmd);

        if (ret != 0) {
            m_logger->error("Received an error code setting PS_MS ") << ret << std::endl;
            return OSVR_RETURN_FAILURE;
        } else {
            m_logger->error("PS_MS Write successful");
        }

        cmd = 0;
        cmd |= 0x01;
        ret = i2c_smbus_write_word_data(file, ALS_CONF, cmd);

        if (ret != 0) {
            m_logger->error("Received an error code ALS_CONF") << ret << std::endl;
            return OSVR_RETURN_FAILURE;
        } else {
            m_logger->info("ALS_CONF Write successful\n");
        }

        int data = i2c_smbus_read_word_data(file, DEV_ID_CMD);
        if (data < 0) {
            m_logger->error("Error reading device id");
        } else {
            if (data != VCNL4040_DEV_ID) {
                m_logger->error("Invalid device ID detected: ") << data << std::endl;
                return OSVR_RETURN_FAILURE;
            }
            m_logger->info("Device id :") << data << std::endl;
        }

        proxSensorDetected = true;
        // Create our device object
        osvr::pluginkit::registerObjectForDeletion(ctx, new ProximitySensorDevice(ctx, file));

        return OSVR_RETURN_SUCCESS;
    }

  private:
    bool proxSensorDetected;
    osvr::util::log::LoggerPtr m_logger;
};

} // namespace

OSVR_PLUGIN(com_sensics_proximity) {

    osvr::pluginkit::PluginContext context(ctx);

    /// Register a detection callback function object.
    context.registerHardwareDetectCallback(new HardwareDetection());

    return OSVR_RETURN_SUCCESS;
}
