/** @file
@brief Android sensor based tracker plugin, currently supporting orientation
only.

@date 2017

@author
Sensics, Inc.
<http://sensics.com/osvr>
*/

// Copyright 2017 Sensics, Inc.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

// Internal Includes
#include <osvr/PluginKit/ButtonInterfaceC.h>
#include <osvr/PluginKit/PluginKit.h>
#include <osvr/Util/Logger.h>

// Generated JSON header file
#include "com_sensics_reed_json.h"

// Library/third-party includes
#include <android/keycodes.h>
#include <android/log.h>

// Standard includes
#include <chrono>
#include <cmath>
#include <iostream>
#include <mutex>
#include <queue>
#include <thread>
#include <utility>

#define LOG_TAG "com_sensics_reed"
#define LOGI(...) __android_log_print(ANDROID_LOG_INFO, LOG_TAG, __VA_ARGS__)
#define LOGE(...) __android_log_print(ANDROID_LOG_ERROR, LOG_TAG, __VA_ARGS__)

#include <jni.h>

// keyboard
#include <errno.h>       // for errno, EACCES, ENOENT
#include <fcntl.h>       // for open, O_RDONLY
#include <linux/input.h> // for input_event, ABS_MAX, etc
#include <map>           // for map, _Rb_tree_iterator, etc
#include <sstream>
#include <string.h>     // for strcmp, NULL, strerror
#include <string>       // for string, operator+, etc
#include <sys/ioctl.h>  // for ioctl
#include <sys/select.h> // for select, FD_ISSET, FD_SET, etc
#include <unistd.h>     // for close, read
#include <utility>      // for pair
#include <fstream>
#include <sys/types.h>
#include <sys/stat.h>
#include <stdio.h>

#include <dirent.h>

namespace {

static std::mutex sInputMutex;
struct ReedSwitchButtonState {
    //@todo do we need an ID
    OSVR_ButtonState state;
    OSVR_TimeValue timestamp;
};

static std::queue<ReedSwitchButtonState> sReedSwitchStateQueueSynced;

//@todo
// call this when the reed switch is flipped
void setReedSwitchState(OSVR_ButtonState state) {
    std::lock_guard<std::mutex> lock(sInputMutex);
    OSVR_TimeValue now;
    osvrTimeValueGetNow(&now);
    ReedSwitchButtonState buttonState = {0};
    buttonState.state = state;
    buttonState.timestamp = now;
    sReedSwitchStateQueueSynced.push(buttonState);
}
} // namespace

// Anonymous namespace to avoid symbol collision
namespace {

class RideVRReedSwitchDevice {
  private:
    osvr::pluginkit::DeviceToken m_dev;
    OSVR_ButtonDeviceInterface m_button;
    ReedSwitchButtonState reedSwitchState;
    bool isSwitchActive = false;
    OSVR_TimeValue lastSwitchTime;
    const char* REED_SWITCH_DEVICE_PATH = "/sys/devices/gpio-keys/input";
    int inputNum = 0; // used to find the name of the input device
    std::string reedSwitchDevicePath;
    osvr::util::log::LoggerPtr m_logger;

  public:
    RideVRReedSwitchDevice(OSVR_PluginRegContext ctx)
        : inputNum(-1), m_logger(osvr::util::log::make_logger("OSVR-Reed")) {
        // @todo sanity check for constructor arguments. All ptrs have to
        // be non-null
        m_logger->trace("RideVRReedSwitchDevice created.");

        /// sys/devices/gpio-keys/input will have one subdirectory.
        struct dirent* de; // Pointer for directory entry

        // opendir() returns a pointer of DIR type.
        DIR* dr = opendir(REED_SWITCH_DEVICE_PATH);

        if (dr == NULL) // opendir returns NULL if couldn't open directory
        {
            m_logger->error("Could not open current directory");
        }

        while ((de = readdir(dr)) != NULL) {
            std::string devName(de->d_name);
            if (devName.find("input") != std::string::npos) {
                // If the subdir is named "inputX", then the device is "/dev/input/eventX"
                char lastChar = devName.back();
                inputNum = lastChar - '0';
                reedSwitchDevicePath = "/dev/input/event" + std::to_string(inputNum); // "/dev/input/mouse0";
                std::string logMsg = "Found Reed Switch at /dev/input/event" + std::to_string(inputNum);
                m_logger->trace(logMsg.c_str());
            }
        }

        closedir(dr);

        if (inputNum < 0) {
            m_logger->error("Could not find input device in /sys/devices/gpio-keys/input/");
        }
        /// Create the initialization options
        OSVR_DeviceInitOptions opts = osvrDeviceCreateInitOptions(ctx);

        /// Configure the button interfaces
        osvrDeviceButtonConfigure(opts, &m_button, 1);
        OSVR_TimeValue now;
        osvrTimeValueGetNow(&now);
        reedSwitchState.timestamp = now;
        lastSwitchTime = now;
        /// Create the sync device token with the options
        m_dev.initAsync(ctx, "ReedSwitch", opts);

        /// Send JSON descriptor
        m_dev.sendJsonDescriptor(com_sensics_reed_json);

        /// Register update callback
        m_dev.registerUpdateCallback(this);
    }

    ~RideVRReedSwitchDevice() {}

    inline const char* const BoolToString(bool b) { return b ? "true" : "false"; }
    void print_event(struct input_event* ie) {
        switch (ie->type) {
        case EV_SYN:
            // Used as markers to separate events. Events may be separated in time or in space,
            // such as with the multitouch protocol.
            // std::cout << "EV_SYN!\n" << std::endl;
            m_logger->trace("EV_SYN");

            //  fprintf(stderr, "time:%ld.%06ld\ttype:%s\tcode:%s\tvalue:%d\n",
            // ie->time.tv_sec, ie->time.tv_usec, ev_type[ie->type],
            // ev_code_syn[ie->code], ie->value);
            break;
        case EV_REL:
            // Used to describe relative axis value changes, e.g. moving the mouse 5 units to the left.
            m_logger->trace("EV_REL");
            break;
        case EV_KEY:
            // Used to describe state changes of keyboards, buttons, or other key - like devices.
            m_logger->trace("EV_KEY");
            break;
        case EV_ABS:
            // Used to describe absolute axis value changes, e.g. describing the coordinates of a touch on a
            // touchscreen.
            m_logger->trace("EV_ABS");
            break;
        case EV_MSC:
            m_logger->trace("EV_MSC");
            break;
        case EV_SW:
            m_logger->trace("EV_SW");
            break;
        case EV_LED:
            m_logger->trace("EV_LED");
            break;
        case EV_SND:
            m_logger->trace("EV_SND");
            break;
        case EV_REP:
            m_logger->trace("EV_REP");
            break;
        case EV_FF:
            m_logger->trace("EV_FF");
            break;
        case EV_PWR:
            m_logger->trace("EV_PWR");
            break;
        case EV_FF_STATUS:
            m_logger->trace("EV_FF_STATUS");
            break;
        default:
            std::string logMsg = "unrecognized event code\ntime_sec : " + std::to_string(ie->time.tv_sec) +
                                 "\ntime_usec: " + std::to_string(ie->time.tv_usec) +
                                 "\nevent_type:" + std::to_string(ie->type) + "\ncode: " + std::to_string(ie->code) +
                                 "\nvalue: " + std::to_string(ie->value);
            m_logger->trace(logMsg.c_str());
            // std::cout << "default!\ntime_sec: " << ie->time.tv_sec << "\ntime_usec: " <<
            // ie->time.tv_usec << "\nevent_type:" << ie->type << "\ncode: " << ie->code << "\nvalue: "
            //<< ie->value << std::endl;

            break;
        }
    }
    bool getReedSwitchActive(const char* reedSwitchDevicePath) {
        // std::cout << "opening " << reedSwitchDevicePath << std::endl;
        std::string logMsg = "Opening ReedSwitch at path: " + std::string(reedSwitchDevicePath);
        m_logger->trace(logMsg.c_str());

        int fd = open(reedSwitchDevicePath, O_RDONLY);
        // std::cout << "opened input device.\n" << std::endl;
        m_logger->trace("Opened ReedSwitch.");
        // The temperature is stored in 5 digits.  The first two are degrees in C.  The rest are decimal precision.
        struct input_event ev;

        read(fd, &ev, sizeof(struct input_event));

        // std::cout << "waiting for input events.\n" << std::endl;

        //@todo read the input event
        // it will be an EV_SW with code / event SW_DOCK
        /*
        EV_SW events describe stateful binary switches. For example, the SW_LID code is used to denote when a
        laptop lid is closed.
        Upon binding to a device or resuming from suspend, a driver must report the current switch state. This
        ensures that the device, kernel, and userspace state is in sync.
        Upon resume, if the switch state is the same as before suspend, then the input subsystem will filter out
        the duplicate switch state reports. The driver does not need to keep the state of the switch at any time.

        struct input_event {
            struct timeval time;
            unsigned short type;
            unsigned short code;
            unsigned int value;
        };

        */
        if (ev.type == EV_SW) {
            // std::cout << "EV_SW input type detected." << std::endl;
            m_logger->trace("EV_SW input type detected.");
            if (ev.code == SW_DOCK) {
                // std::cout << "SW_DOCK input code detected. Value = " << ev.value << std::endl;
                logMsg = "SW_DOCK input code detected. Value = " + std::to_string(ev.value);
                m_logger->trace(logMsg.c_str());
                /// GPIO reports value of 1 when switch is opened
                /// When reed switch is closed, value is 0
                if (ev.value == 1) {
                    isSwitchActive = false;
                } else {
                    isSwitchActive = true;
                }
            }
        }

        // print_event(&ev);

        close(fd);

        return isSwitchActive;
    }

    OSVR_ReturnCode update() {
        // std::cout << "[com_sensics_reed]: update" << std::endl;
        /*OSVR_ReturnCode buttonRet = reportReedSwitchButtonState(m_dev, m_button);

        bool ret = buttonRet == OSVR_RETURN_SUCCESS;
        return ret ? OSVR_RETURN_SUCCESS : OSVR_RETURN_FAILURE;*/

        OSVR_TimeValue now;
        osvrTimeValueGetNow(&now);
        reedSwitchState.timestamp = now;

        if (inputNum >= 0) {
            // std::cout << "getReedSwitchActive.\n" << std::endl;

            bool isReedSwitchActive = getReedSwitchActive(reedSwitchDevicePath.c_str());

            if (OSVR_TRUE == isReedSwitchActive) {
                reedSwitchState.state = OSVR_BUTTON_PRESSED;
            } else {
                reedSwitchState.state = OSVR_BUTTON_NOT_PRESSED;
            }
            // std::cout << "send button.\n" << std::endl;

            if (OSVR_RETURN_SUCCESS != osvrDeviceButtonSetValue(m_dev, m_button, reedSwitchState.state, 0)) {
                // LOGE("[com_sensics_reed]: Failed to send button state.");
                m_logger->error("[com_sensics_reed]: Failed to send button state.");
                // std::cout << "[com_sensics_reed]: Failed to send button state."
                //<< std::endl;
                return OSVR_RETURN_FAILURE;
            }
            // std::cout << "[com_sensics_reed]: Sent ReedSwitch State: " <<
            // BoolToString(isReedSwitchActive) << std::endl;
            // std::cout << "button sent" << std::endl;
            std::string logMsg = "Sent ReedSwitch State: " + std::string(BoolToString(isReedSwitchActive));
            m_logger->trace(logMsg.c_str());
            return OSVR_RETURN_SUCCESS;
        }

        m_logger->error("No input number.");

        return OSVR_RETURN_FAILURE;

        // DUMMY DATA
        /*
        if (now.seconds - lastSwitchTime.seconds > 3) {
        lastSwitchTime = now;
        //Toggle the button
        if (OSVR_TRUE == buttonPressed) {
        std::cout << "[com_sensics_reed]: ReedSwitch State = 0" << std::endl;
        buttonPressed = OSVR_FALSE;
        reedSwitchState.state = OSVR_BUTTON_NOT_PRESSED;
        } else {
        std::cout << "[com_sensics_reed]: ReedSwitch State = 1" << std::endl;
        buttonPressed = OSVR_TRUE;
        reedSwitchState.state = OSVR_BUTTON_PRESSED;
        }

        if (OSVR_RETURN_SUCCESS !=
        osvrDeviceButtonSetValue(m_dev, m_button, reedSwitchState.state, 0)) {
        LOGE("[com_sensics_reed]: Failed to send button state.");
        std::cout << "[com_sensics_reed]: Failed to send button state."
        << std::endl;
        return OSVR_RETURN_FAILURE;
        } else {
        // std::cout << "[com_sensics_reed]: Sent ReedSwitch State: " <<
        // BoolToString(buttonPressed) << std::endl;
        return OSVR_RETURN_SUCCESS;
        }
        } else {
        return OSVR_RETURN_SUCCESS;
        }
        */
    }
};

class HardwareDetection {
  public:
    HardwareDetection() : mDeviceAdded(false), m_logger(osvr::util::log::make_logger("OSVR-Reed")) {}
    OSVR_ReturnCode operator()(OSVR_PluginRegContext ctx) {

        if (!mDeviceAdded) {

            // LOGI("[com_sensics_reed]: Reed Switch plugin enabled!!");
            // std::cout << "[com_sensics_reed]: Reed Switch plugin enabled!!"
            //<< std::endl;
            m_logger->trace("ReedSwitch plugin enabled");

            /// Create our device object
            osvr::pluginkit::registerObjectForDeletion(ctx, new RideVRReedSwitchDevice(ctx));

            // don't add the same device twice or more
            mDeviceAdded = true;
        }
        return OSVR_RETURN_SUCCESS;
    }

  private:
    bool mDeviceAdded = false;
    osvr::util::log::LoggerPtr m_logger;
};
} // namespace

OSVR_PLUGIN(com_sensics_reed) {
    // std::cout << "[com_sensics_reed]: Entry point executed!" << std::endl;
    // LOGI("[com_sensics_reed]: Entry point executed!");
    // m_logger->trace("ReedSwitch plugin enabled");
    osvr::pluginkit::PluginContext context(ctx);

    /// Register a detection callback function object.
    context.registerHardwareDetectCallback(new HardwareDetection());

    return OSVR_RETURN_SUCCESS;
}
