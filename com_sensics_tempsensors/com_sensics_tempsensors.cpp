/** @file
@brief Android sensor based tracker plugin, currently supporting orientation
only.

@date 2017

@author
Sensics, Inc.
<http://sensics.com/osvr>
*/

// Copyright 2017 Sensics, Inc.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

// Internal Includes
#include <osvr/PluginKit/AnalogInterfaceC.h>
#include <osvr/PluginKit/PluginKit.h>
#include <osvr/Util/Logger.h>

// Generated JSON header file
#include "com_sensics_tempsensors_json.h"

// Library/third-party includes
// - none

// Standard includes
#include <array>
#include <iostream>
#include <fstream>
#include <string>

// Anonymous namespace to avoid symbol collision
namespace {

static const auto NUM_SENSORS = 8;

struct AnalogReport {
    OSVR_TimeValue timestamp;
    OSVR_ChannelCount sensor;
    double value;
};

class RideVRTempSensorsDevice {

  private:
    osvr::pluginkit::DeviceToken m_dev;
    OSVR_AnalogDeviceInterface m_analog;
    OSVR_AnalogState m_analogStates[NUM_SENSORS];
    const std::array<std::string, NUM_SENSORS> TEMP_SENSOR_PATHS = {
        "/sys/class/thermal/thermal_zone0/temp", "/sys/class/thermal/thermal_zone1/temp",
        "/sys/class/thermal/thermal_zone2/temp", "/sys/class/thermal/thermal_zone3/temp",
        "/sys/class/thermal/thermal_zone4/temp", "/sys/class/thermal/thermal_zone5/temp",
        "/sys/class/thermal/thermal_zone6/temp", "/sys/class/thermal/thermal_zone7/temp",
    };
    osvr::util::log::LoggerPtr m_logger;
    std::string const& getTempSensorPath(int sensorNumber) { return TEMP_SENSOR_PATHS[sensorNumber]; }

  public:
    RideVRTempSensorsDevice(OSVR_PluginRegContext ctx) : m_logger(osvr::util::log::make_logger("OSVR-TempSensors")) {
        /// @todo switch this logger to actually use the C API in pluginkit
        m_logger->trace("RideVRTempSensorsDevice created.");

        /// Create the initialization options
        OSVR_DeviceInitOptions opts = osvrDeviceCreateInitOptions(ctx);

        /// Configure the button interfaces
        osvrDeviceAnalogConfigure(opts, &m_analog, NUM_SENSORS);

        /// Create the sync device token with the options
        m_dev.initAsync(ctx, "TemperatureSensors", opts);

        /// Send JSON descriptor
        m_dev.sendJsonDescriptor(com_sensics_tempsensors_json);

        /// Register update callback
        m_dev.registerUpdateCallback(this);
    }

    ~RideVRTempSensorsDevice() {}

    double getCurrentTemperature(std::string const& sensorPath) {
        std::string val;
        std::string preparedTemp;
        double temperature;

        std::ifstream temperatureFile(sensorPath);

        if (!temperatureFile.good()) {
            m_logger->error("Failed to open temperatureFile.");
            return -1;
        }

        // The temperature is stored in 5 digits.  The first two are degrees in C.  The rest are decimal precision.
        temperatureFile >> val;

        temperatureFile.close();

        preparedTemp = val.insert(2, 1, '.');
        temperature = std::stod(preparedTemp);

        return temperature;
    }

    OSVR_ReturnCode update() {

        for (int i = 0; i < NUM_SENSORS; i++) {
            double temp = getCurrentTemperature(getTempSensorPath(i));
            m_analogStates[i] = temp;
        }
        OSVR_TimeValue tempCheckTime = osvr::util::time::getNow();

        osvrDeviceAnalogSetValuesTimestamped(m_dev, m_analog, m_analogStates, NUM_SENSORS, &tempCheckTime);
        return OSVR_RETURN_SUCCESS;
    }
};

class HardwareDetection {
  public:
    HardwareDetection() : mDeviceAdded(false), m_logger(osvr::util::log::make_logger("OSVR-TempSensors")) {}
    OSVR_ReturnCode operator()(OSVR_PluginRegContext ctx) {

        if (!mDeviceAdded) {
            m_logger->trace("TempSensors hardware detected.");
            /// Create our device object
            osvr::pluginkit::registerObjectForDeletion(ctx, new RideVRTempSensorsDevice(ctx));

            // don't add the same device twice or more
            mDeviceAdded = true;
        }
        return OSVR_RETURN_SUCCESS;
    }

  private:
    bool mDeviceAdded = false;
    osvr::util::log::LoggerPtr m_logger;
};
} // namespace

OSVR_PLUGIN(com_sensics_tempsensors) {
    osvr::pluginkit::PluginContext context(ctx);

    /// Register a detection callback function object.
    context.registerHardwareDetectCallback(new HardwareDetection());

    return OSVR_RETURN_SUCCESS;
}
